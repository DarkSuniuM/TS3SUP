from wtforms import StringField, PasswordField, validators, BooleanField, SelectField, HiddenField, Form
from flask_wtf import FlaskForm, CSRFProtect

csrf = CSRFProtect()


class AddUser(FlaskForm):
    username = StringField("Username",
                           validators=[validators.DataRequired(), validators.Length(min=4, max=32)])
    email = StringField("Email",
                        validators=[validators.DataRequired(), validators.Email()])
    password = PasswordField("Password",
                             validators=[validators.DataRequired(), validators.Length(min=8), validators.EqualTo("password_confirm")])
    password_confirm = PasswordField("Password Confirm")
    active = BooleanField("Activate")
    admin = BooleanField("Admin")
    server_limit = StringField("Server Limit",
                               validators=[validators.DataRequired()])
    slot_limit = StringField("Slot Limit",
                             validators=[validators.DataRequired()])


class ModifyUser(AddUser):
    password = PasswordField("Password",
                             validators=[validators.Optional(),
                                         validators.Length(min=8),
                                         validators.EqualTo("password_confirm")])
    password_confirm = PasswordField("Password Confirm")
    pass


class SearchUser(FlaskForm):
    search_query = StringField("Search Query",
                               validators=[validators.DataRequired()])
    search_in = SelectField("Search in",
                            validators=[validators.DataRequired()],
                            choices=[('user_id', 'IDs'), ('username', 'Usernames'), ('email', 'Emails')])


class SettingsUser(FlaskForm):
    email = StringField("Email",
                        validators=[validators.DataRequired(),
                                    validators.Email()])
    old_password = PasswordField("Current Password")
    new_password = PasswordField("New Password",
                                 validators=[validators.Optional(),
                                             validators.Length(min=8),
                                             validators.EqualTo("new_password_confirm")])
    new_password_confirm = PasswordField("New Password Confirm")


class AddServer(FlaskForm):
    server_ip = StringField("IP",
                            validators=[validators.DataRequired(),
                                        validators.IPAddress()])
    server_port = StringField("Query Port",
                              validators=[validators.DataRequired(),
                                          validators.NumberRange()],
                              default=10011)
    server_login = StringField("Query Login",
                               validators=[validators.DataRequired()],
                               default="serveradmin")
    server_password = PasswordField("Query Password",
                                    validators=[validators.DataRequired()])


class ModifyServer(AddServer):
    server_password = PasswordField("Query Password",
                                    validators=[validators.Optional()])


class SearchServer(FlaskForm):
    search_query = StringField("Search Query",
                               validators=[validators.DataRequired()])
    search_in = SelectField("Search in",
                            validators=[validators.DataRequired()],
                            choices=[('server_id', 'IDs'), ('server_ip', 'IPs')])


class AddTeamSpeak(FlaskForm):

    def set_choices(self):
        from Models import db, Server
        self.teamspeak_server.choices = [(int(server[0]), str(server[1])) for server in db.session.query(Server.server_id, Server.server_ip).all()]

    teamspeak_server = SelectField("Server",
                                   validators=[validators.DataRequired()], coerce=int)
    teamspeak_port = StringField("Port",
                                 validators=[validators.DataRequired()])
    teamspeak_slot = StringField("Slot",
                                 validators=[validators.DataRequired()])


class ModifyTeamSpeak(FlaskForm):
    teamspeak_port = StringField("Port",
                                 validators=[validators.DataRequired()])
    teamspeak_slot = StringField("Slot",
                                 validators=[validators.DataRequired()])


class SearchTeamSpeak(SearchServer):
    search_query = StringField("Search Query",
                               validators=[validators.DataRequired()])
    search_in = SelectField("Search in",
                            validators=[validators.DataRequired()],
                            choices=[('ts_id', 'IDs'), ('ts_server', 'IPs'), ('ts_sid', 'SIDs'), ('ts_port', 'Port')])
