from TS3SuperUserPanel import db

db.sessionmaker(autoflush=True)

class User(db.Model):
    __tablename__ = "user"
    user_id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), unique=True, nullable=False)
    email = db.Column(db.String(50), unique=True, nullable=False)
    password = db.Column(db.String(77), unique=False, nullable=False)
    is_active = db.Column(db.Boolean, unique=False, nullable=False, server_default="0")
    is_admin = db.Column(db.Boolean, unique=False, nullable=False, server_default="0")
    server_limit = db.Column(db.Integer, unique=False, nullable=False, server_default="4")
    slot_limit = db.Column(db.Integer, unique=False, nullable=False, server_default="128")
    register_date = db.Column(db.DateTime, unique=False, nullable=False, server_default=db.func.now())
    update_date = db.Column(db.DateTime, unique=False, nullable=True, server_onupdate=db.func.now())
    servers = db.relationship('Server', backref='owner', lazy='dynamic')
    teamspeaks = db.relationship('TeamSpeak', backref='admin', lazy='dynamic')

    def __init__(self, username, email, password, is_active=0, is_admin=0, server_limit=4, slot_limit=128):
        self.username = username
        self.email = email
        self.password = password
        self.is_active = is_active
        self.is_admin = is_admin
        self.server_limit = server_limit
        self.slot_limit = slot_limit

    def used(self):
        # return db.session.execute("SELECT SUM(teamspeak.ts_slot) FROM teamspeak WHERE ts_admin={}".format(self.user_id)).scalar()
        try:
            return int(db.session.query(db.func.sum(TeamSpeak.ts_slot)).filter_by(ts_admin=self.user_id).scalar())
        except TypeError:
            return 0


class Server(db.Model):
    __tablename__ = "server"
    server_id = db.Column(db.Integer, primary_key=True)
    server_owner = db.Column(db.Integer, db.ForeignKey("user.user_id"))
    server_queryport = db.Column(db.Integer, unique=False, nullable=False, server_default="10011")
    server_admin = db.Column(db.String(16), unique=False, nullable=False, server_default="serveradmin")
    server_ip = db.Column(db.String(15), unique=True, nullable=False, server_default="127.0.0.1")
    server_secure = db.Column(db.String(24), unique=False, nullable=False)
    teamspeaks = db.relationship('TeamSpeak', backref='server', lazy='dynamic')

    def __init__(self, server_owner, server_ip, server_secure, server_queryport="10011", server_admin="serveradmin"):
        self.server_owner = server_owner
        self.server_ip = server_ip
        self.server_queryport = server_queryport
        self.server_admin = server_admin
        self.server_secure = server_secure

    def used(self):
        # return db.session.execute("SELECT SUM(teamspeak.ts_slot) FROM teamspeak WHERE ts_server={}".format(self.server_id)).scalar()
        try:
            return int(db.session.query(db.func.sum(TeamSpeak.ts_slot)).filter_by(ts_server=self.server_id).scalar())
        except TypeError:
            return 0

    def server_count(self):
        return db.session.query(db.func.count(TeamSpeak.ts_id)).filter_by(ts_server=self.server_id).scalar()


class TeamSpeak(db.Model):
    __tablename__ = "teamspeak"
    ts_id = db.Column(db.Integer, primary_key=True)
    ts_server = db.Column(db.Integer, db.ForeignKey("server.server_id"))
    ts_sid = db.Column(db.Integer, unique=False, nullable=False)
    ts_admin = db.Column(db.Integer, db.ForeignKey("user.user_id"))
    ts_port = db.Column(db.Integer, unique=False, nullable=False)
    ts_slot = db.Column(db.Integer, unique=False, nullable=False, server_default="32")
    ts_date = db.Column(db.DateTime, unique=False, nullable=False, server_default=db.func.now())
    ts_status = db.Column(db.Boolean, unique=False, nullable=False)
    __table_args__ = (db.UniqueConstraint('ts_server', 'ts_sid', name='ts_con_unique'),
                      )

    def __init__(self, ts_server, ts_admin, ts_port, ts_slot, ts_sid, ts_status):
        self.ts_server = ts_server
        self.ts_admin = ts_admin
        self.ts_port = ts_port
        self.ts_slot = ts_slot
        self.ts_sid = ts_sid
        self.ts_status = ts_status