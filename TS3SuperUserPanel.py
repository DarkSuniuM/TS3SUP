#!/usr/bin/python3
from flask import Flask, render_template, redirect, url_for, flash, current_app, request, session, logging
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import IntegrityError
from passlib.hash import sha256_crypt
from functools import wraps
from ts3py import TS3Query, TS3Error
from datetime import datetime as time_now
from libgravatar import Gravatar
from Forms import *


app = Flask(__name__)
# CONFIG #
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SECRET_KEY'] = 'This is secret)'
# # MySQL Config # #
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:secret@127.0.0.1:3306/TS3SUP?charset=utf8'
db = SQLAlchemy(app, session_options={"expire_on_commit": True})
from Models import *
app.config['CSRF_ENABLED'] = True
csrf.init_app(app)
app.config['USER_ENABLE_EMAIL'] = False


# Wrappers
def is_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('You need to log in first!', 'danger')
            return redirect(url_for('login'))
    return wrap


# Error Pages
@app.errorhandler(404)
def not_found(e):
    return render_template('404.html'), 404


# Login
@app.route('/', methods=['GET', 'POST'])
@app.route('/login/', methods=['GET', 'POST'])
def login():
    if "logged_in" in session:  # If User already logged in
        return redirect(url_for('dashboard'))

    elif request.method == 'POST':  # Else If User is trying to Login
        username = request.form['username']
        password_candidate = str(request.form['pass'])
        log_user = db.session.query(User).filter_by(username=username).first()

        if log_user is not None:  # If User Found

            if sha256_crypt.verify(password_candidate, log_user.password):  # If Login(Password) is Correct

                if log_user.is_active:  # If User is Active
                    gravatar = Gravatar(log_user.email)
                    session['logged_in'] = True
                    session['username'] = log_user.username
                    session['user_id'] = log_user.user_id
                    session['is_admin'] = log_user.is_admin
                    session['avatar'] = gravatar.get_image()
                    flash('You logged in successfully!', 'success')
                    return redirect(url_for('dashboard'))

                else:  # If User is Not Active
                    flash('Your Account is not Activated, Contact Administrator!', 'danger')
                    return render_template('login.html')

            else:  # If Login(Password) is Not Correct
                flash('Login Incorrect!', 'danger')
                return render_template('login.html')

        else:  # If User Not Found
            flash('Login Incorrect!', 'danger')
            return render_template('login.html')

    return render_template('login.html')


# Dashboard
@app.route('/dashboard/')
@is_logged_in
def dashboard():
    if session['is_admin']:
        counts = (db.session.query(Server.server_id).count(), db.session.query(User.user_id).count(), db.session.query(TeamSpeak.ts_id).count())
        lastest_teamspeaks = db.session.query(TeamSpeak).order_by(TeamSpeak.ts_id.desc()).limit(5).all()
        return render_template('dashboard.html', title="Dashboard", stats=counts, teamspeaks=lastest_teamspeaks)
    else:
        user = db.session.query(User).filter_by(user_id=session['user_id']).first()
        lastest_teamspeaks = db.session.query(TeamSpeak).filter_by(ts_admin=session['user_id']).limit(5).all()
        counts = (user.teamspeaks.count(), user.server_limit, user.used(), user.slot_limit)
        return render_template('dashboard.html', title="Dashboard", stats=counts, teamspeaks=lastest_teamspeaks)


# User Settings
@app.route('/settings/', methods=['GET', 'POST'])
@is_logged_in
def settings():
    user = db.session.query(User).filter_by(user_id=session['user_id']).first()
    modify_form = SettingsUser(request.form)
    if request.method == 'POST' and 'save_changes' in request.form:
        if modify_form.validate():
            if sha256_crypt.verify(str(modify_form.old_password.data), user.password):
                user.email = modify_form.email.data
                if modify_form.new_password.data:
                    user.password = sha256_crypt.hash(str(modify_form.new_password.data))
                try:
                    db.session.commit()
                except IntegrityError as e:
                    db.session.rollback()
                    flash(str(e.args[0]), "danger")
                    return render_template('settings.html', title="User Settings", modify=modify_form, user=user)
                flash("Changes saved!", "success")
                return render_template('settings.html', title="User Settings", modify=modify_form, user=user)
            else:
                flash("Current password is wrong!", "danger")
                return render_template('settings.html', title="User Settings", modify=modify_form, user=user)
    elif request.method == 'POST' and 'cance' in request.form:
        flash("Modify aborted!", "info")
        return redirect(url_for('dashboard'))

    return render_template('settings.html', title="User Settings", modify=modify_form, user=user )


# Users List
@app.route('/users/', methods=['GET', 'POST'])
@is_logged_in
def users():
    if not session["is_admin"]:  # If User is not Authorized
        flash("Access Denied!", "danger")
        return redirect(url_for('dashboard'))

    users_list = db.session.query(User).all()
    reg_form = AddUser(request.form)
    search_form = SearchUser(request.form)

    if request.method == 'POST' and 'add_sub' in request.form:  # If Add Submit Clicked.
        if reg_form.validate():  # If Form is Validated
            new_user = User(username=reg_form.username.data,
                            email=reg_form.email.data,
                            password=sha256_crypt.hash(str(reg_form.password.data)),
                            is_active=reg_form.active.data,
                            is_admin=reg_form.admin.data,
                            server_limit=int(reg_form.server_limit.data),
                            slot_limit=int(reg_form.slot_limit.data))
            try:  # Try to Add User
                db.session.add(new_user)
                db.session.commit()
                flash("User got added successfully!", "success")
                users_list = db.session.query(User).all()

            except IntegrityError as e:  # Except ( User May Exists )
                db.session.rollback()
                flash(str(e.args[0]), "danger")

        else:  # If Form is not Validated
            flash("Something went wrong!", "danger")

    elif request.method == 'POST' and 'search_sub' in request.form:  # If Search Submit Clicked.
        if search_form.validate():  # If form is Validated
            if search_form.search_in.data == 'user_id':  # If Searching in IDs
                users_list = db.session.query(User).filter_by(user_id=search_form.search_query.data).all()
            elif search_form.search_in.data == 'username':  # If Searching in Usernames
                users_list = db.session.query(User).filter_by(username=search_form.search_query.data).all()
            elif search_form.search_in.data == 'email':  # If Searching in Emails
                users_list = db.session.query(User).filter_by(email=search_form.search_query.data).all()
        else:  # If Form is not Validated
            flash("Something went wrong!", "danger")

    return render_template('users.html', title="Users", users=users_list, reg=reg_form, search=search_form)


# Edit a User
@app.route('/users/edit/<user_id>/', methods=['GET', 'POST'])
@is_logged_in
def edit_user(user_id):
    if not session["is_admin"]:  # If the user who trying to reach the page is not Admin
        flash("Access Denied!", "danger")
        return redirect(url_for('dashboard'))

    user = db.session.query(User).filter_by(user_id=user_id).first()
    if user:
        pass
    else:
        flash("User not found!", "danger")
        return redirect(url_for("users"))

    modify_form = ModifyUser()
    if request.method == 'POST' and 'modify_user' in request.form:  # If Modify User Clicked.
        if modify_form.validate():  # If Modify Form Validated
            user.username = modify_form.username.data
            user.email = modify_form.email.data
            if modify_form.password.data is not "":  # If Password Changes
                user.password = sha256_crypt.hash(str(modify_form.password.data))
            user.server_limit = modify_form.server_limit.data
            user.slot_limit = modify_form.slot_limit.data
            user.update_date = db.func.now()
            try:  # Try to Commit Changes
                db.session.commit()
                flash("User Modified Successfully!", "success")
                return redirect(url_for('users'))
            except IntegrityError as e:  # Except Another User With Same Mail or Username
                db.session.rollback()
                flash(e.args[0], "danger")
        else:  # If Modify Form is not Validated
            flash("Something went wrong!", "danger")

    elif request.method == 'POST' and 'cancel' in request.form:  # If Cancel Button Clicked.
        flash("Modify aborted successfully", "info")
        return redirect(url_for('users'))

    return render_template('edituser.html', title="Edit ", user=user, modify=modify_form)


# Activate a User
@app.route('/users/activate/<user_id>')
@is_logged_in
def activate_user(user_id):
    if not session["is_admin"]:  # If the user who trying to reach the page is not Admin
        flash("Access Denied!", "danger")
        return redirect(url_for('dashboard'))

    user = db.session.query(User).filter_by(user_id=user_id).first()
    if user is None:
        flash("User not found!", "danger")
    elif user.is_active == 1:
        flash("User is already active.", "danger")
    else:
        user.is_active = 1
        try:
            db.session.commit()
            flash("User is activated!", "success")
        except Exception as e:
            db.session.rollback()
            flash(str(e), "danger")
    return redirect(url_for('users'))


# Deactivate a User
@app.route('/users/deactivate/<user_id>')
@is_logged_in
def deactivate_user(user_id):
    if not session["is_admin"]:  # If the user who trying to reach the page is not Admin
        flash("Access Denied!", "danger")
        return redirect(url_for('dashboard'))

    user = db.session.query(User).filter_by(user_id=user_id).first()
    if user is None:
        flash("User not found!", "danger")
    elif user.is_active == 0:
        flash("User is not active.", "danger")
    elif user.is_admin:
        flash("You can't deactivate an Admin account!", "danger")
    else:
        user.is_active = 0
        try:
            db.session.commit()
            flash("User is deactivated!", "success")
        except Exception as e:
            db.session.rollback()
            flash(str(e), "danger")
    return redirect(url_for('users'))


# Delete a User
@app.route('/users/delete/<user_id>')
@is_logged_in
def delete_user(user_id):
    if not session["is_admin"]:  # If the user who trying to reach the page is not Admin
        flash("Access Denied!", "danger")
        return redirect(url_for('dashboard'))

    user = db.session.query(User).filter_by(user_id=user_id).first()
    if user is None:
        flash("User not found!", "danger")
    elif user.is_admin:
        flash("You can't delete an Admin account!", "danger")
    else:
        db.session.query(User).filter_by(user_id=user.user_id).delete()
        try:
            db.session.commit()
            flash("User deleted successfully", "success")
        except Exception as e:
            db.session.rollback()
            flash(str(e), "danger")
    return redirect(url_for('users'))


# Logout
@app.route('/logout/')
@is_logged_in
def logout():
    session.clear()
    flash('You logged out successfully', 'info')
    return redirect(url_for('login'))


# Servers List
@app.route('/servers/', methods=['GET', 'POST'])
@is_logged_in
def servers():
    if not session["is_admin"]:  # If User is not Authorized
        flash("Access Denied!", "danger")
        return redirect(url_for('dashboard'))

    search_form = SearchServer(request.form)
    add_form = AddServer(request.form)
    servers_list = db.session.query(Server).all()

    if request.method == 'POST' and 'add_sub' in request.form:  # If Add Submit Clicked.
        if add_form.validate():  # If Form is Validated
            try:  # Try to check TS Query Login Info
                ts_query = TS3Query(ip=add_form.server_ip.data,
                                    port=int(add_form.server_port.data))
                ts_query.login(username=add_form.server_login.data,
                               password=add_form.server_password.data)
            except (TS3Error, TimeoutError) as e:  # If Error
                flash(str(e), "danger")
                return render_template('servers.html', title="Servers", servers=servers_list, search=search_form, add=add_form)

            owner = db.session.query(User.user_id).filter_by(username=session['username']).first()
            new_server = Server(server_owner=owner[0],
                                server_ip=str(add_form.server_ip.data),
                                server_queryport=str(add_form.server_port.data),
                                server_admin=str(add_form.server_login.data),
                                server_secure=str(add_form.server_password.data))
            try:  # Try to Add Server
                db.session.add(new_server)
                db.session.commit()
                ts_servers = ts_query.serverlist()
                teamspeaks = []
                for ts in ts_servers.keys():
                    if 'virtualserver_maxclients' in ts_servers[ts]:
                        slots = ts_servers[ts]['virtualserver_maxclients']
                        status = 1
                    else:
                        slots = 0
                        status = 0
                    teamspeaks.append({"ts_server": new_server.server_id,
                                       "ts_sid": ts,
                                       "ts_port": ts_servers[ts]['virtualserver_port'],
                                       "ts_admin": owner[0],
                                       "ts_slot": slots,
                                       "ts_status": status})
                db.session.bulk_insert_mappings(TeamSpeak, teamspeaks)
                try:  # Add TeamSpeak Servers on This Server
                    db.session.commit()
                except IntegrityError as e:  # If Error, Rollback and Flash The Error
                    db.session.rollback()
                    flash(str(e), "danger")

                flash("Server got added successfully!", "success")
                servers_list = db.session.query(Server).all()

            except IntegrityError as e:  # Except ( Server May Exists )
                db.session.rollback()
                flash(str(e.args[0]), "danger")

        else:  # If Form is not Validated
            flash("Something went wrong!", "danger")

    elif request.method == 'POST' and 'search_sub' in request.form:  # If Search Submit Clicked.
        if search_form.validate():  # If form is Validated
            if search_form.search_in.data == 'server_id':  # If Searching in IDs
                servers_list = db.session.query(Server).filter_by(server_id=search_form.search_query.data).all()
            elif search_form.search_in.data == 'server_ip':  # If Searching in Usernames
                servers_list = db.session.query(Server).filter_by(server_ip=search_form.search_query.data).all()
        else:  # If Form is not Validated
            flash("Something went wrong!", "danger")

    return render_template('servers.html', title="Servers", servers=servers_list, search=search_form, add=add_form)


# Edit a Server
@app.route('/servers/edit/<server_id>', methods=['GET', 'POST'])
@is_logged_in
def edit_server(server_id):
    if not session["is_admin"]:  # If the user who trying to reach the page is not Admin
        flash("Access Denied!", "danger")
        return redirect(url_for('dashboard'))

    server = db.session.query(Server).filter_by(server_id=server_id).first()
    if server:
        pass
    else:
        flash("Server not found!", "danger")
        return redirect(url_for("servers"))

    modify_form = ModifyServer()
    if request.method == 'POST' and 'modify_server' in request.form:  # If Modify Server Clicked.
        if modify_form.validate():  # If Modify Form Validated
            server.server_ip = modify_form.server_ip.data
            server.server_queryport = modify_form.server_port.data
            server.server_admin = modify_form.server_login.data
            if modify_form.server_password.data is not "":
                server.server_secure = modify_form.server_password.data
            try:
                ts_query = TS3Query(server.server_ip, server.server_queryport)
                try:
                    ts_query.login(server.server_admin, server.server_secure)
                    try:  # Try to Commit Changes
                        db.session.commit()
                        flash("Server Modified Successfully!", "success")
                        return redirect(url_for('servers'))
                    except IntegrityError as e:  # Except Another User With Same Mail or Username
                        db.session.rollback()
                        flash(e.args[0], "danger")
                except TS3Error as e:
                    flash(str(e), "danger")
            except TimeoutError as e:
                flash(str(e), "danger")
                flash("Connection Timed Out", "danger'")

        else:  # If Modify Form is not Validated
            flash("Something went wrong!", "danger")

    elif request.method == 'POST' and 'cancel' in request.form:  # If Cancel Button Clicked.
        flash("Modify aborted successfully", "info")
        return redirect(url_for('users'))

    return render_template('editserver.html', title="Edit ", server=server, modify=modify_form)


# Delete a server
@app.route('/servers/delete/<server_id>')
@is_logged_in
def delete_server(server_id):
    if not session["is_admin"]:  # If the server who trying to reach the page is not Admin
        flash("Access Denied!", "danger")
        return redirect(url_for('dashboard'))

    server = db.session.query(Server).filter_by(server_id=server_id).first()
    if server is None:
        flash("Server not found!", "danger")
    else:
        db.session.query(TeamSpeak).filter_by(ts_server=server.server_id).delete()
        db.session.query(Server).filter_by(server_id=server.server_id).delete()
        try:
            db.session.commit()
            flash("Server deleted successfully", "success")
        except Exception as e:
            db.session.rollback()
            flash(str(e), "danger")
    return redirect(url_for('servers'))


# Update TeamSpeak List
@app.route('/cron/updateservers')
def update_servers():
    servers = db.session.query(Server).all()
    for server in servers:
        ts_query = TS3Query(server.server_ip, server.server_queryport)
        try:
            ts_query.login(server.server_admin, server.server_secure)
        except TS3Error as e:
            with open("error.log", "w") as log:
                logs = log.read().splitlines()
                logs.append("[{error_date}] | Error | {error_message}\n".format(
                    error_date=str(time_now.now()),
                    error_message=str(e)))
                log.writelines(logs)
                log.close()
            continue
        ts_servers = ts_query.serverlist()
        ts_servers_on_db = db.session.query(TeamSpeak).filter_by(ts_server=server.server_id).all()
        for ts_server in ts_servers.keys():
            if 'virtualserver_maxclients' in ts_servers[ts_server]:
                        slots = ts_servers[ts_server]['virtualserver_maxclients']
                        status = 1
            else:
                slots = 0
                status = 0
            teamspeak = TeamSpeak(ts_server=server.server_id,
                                  ts_sid=ts_server,
                                  ts_port=ts_servers[ts_server]['virtualserver_port'],
                                  ts_admin=server.server_owner,
                                  ts_slot=slots,
                                  ts_status=status)
            db.session.add(teamspeak)
            try:
                db.session.commit()
            except IntegrityError as e:
                db.session.rollback()
                teamspeak = db.session.query(TeamSpeak).filter_by(ts_server=server.server_id, ts_sid=ts_server).first()
                teamspeak.ts_port = ts_servers[ts_server]['virtualserver_port']
                teamspeak.ts_slot = slots
                teamspeak.ts_status = status
                try:
                    db.session.commit()
                except Exception as e:
                    db.session.rollback()
                    with open("error.log", "w") as log:
                        logs = log.read().splitlines()
                        logs.append("[{error_date}] | Error | {error_message}\n".format(
                            error_date=str(time_now.now()),
                            error_message=str(e)))
                        log.writelines(logs)
                        log.close()
                    continue
        ts_query.disconnect()
        for ts_server in ts_servers_on_db:
            if ts_server.ts_sid in ts_servers.keys():
                continue
            else:
                db.session.query(TeamSpeak).filter_by(ts_sid=ts_server.ts_sid).delete()
                db.session.commit()

    return "<p>Updated!</p>"


# TeamSpeak List
@app.route('/teamspeak/', methods=['GET', 'POST'])
@is_logged_in
def teamspeaks():
    if session['is_admin']:  # If User is Admin
        teamspeaks = db.session.query(TeamSpeak).all()  # Show All TeamSpeaks!
    else:  # If User is Reseller
        teamspeaks = db.session.query(TeamSpeak).filter_by(ts_admin=session['user_id']).all()  # Show His TeamSpeaks

    add_teamspeak = AddTeamSpeak(request.form)  # Add a New TeamSpeak Form
    add_teamspeak.set_choices()
    search_form = SearchTeamSpeak(request.form)  # Search in TeamSpeaks Form

    if request.method == 'POST' and 'add_ts' in request.form:  # If Method is Post & Add TeamSpeak Clicked
        if add_teamspeak.validate():  # If Form Data is Valid
            admin = db.session.query(User).filter_by(user_id=session['user_id']).first()  # TS Owner or Admin
            # [ Next Line If ]
            #  1.If User has not reached his Server Count Limitation
            #  AND
            #  2. If User has not reached his Server slot Limitation
            #  OR
            #  User is Admin
            if (
                    ((int(admin.teamspeaks.count()) < int(admin.server_limit)) or (int(admin.server_limit) == -1))
                    and
                    ((int(admin.used() + int(add_teamspeak.teamspeak_slot.data)) <= int(admin.slot_limit)) or (int(admin.slot_limit) == -1)))\
                    or \
                    (session['is_admin']):
                try:
                    server = db.session.query(Server).filter_by(server_id=add_teamspeak.teamspeak_server.data).first()  # TeamSpeak Server
                    ts_query = TS3Query(server.server_ip, server.server_queryport)  # Try to Connect to Telnet
                    ts_query.login(server.server_admin, server.server_secure)  # Try to Login
                    cmd = "servercreate virtualserver_name=TeamSpeak\s]\p[\sServer virtualserver_port={ts_port} virtualserver_maxclients={ts_slot}".format(ts_port=str(add_teamspeak.teamspeak_port.data), ts_slot=str(add_teamspeak.teamspeak_slot.data))  # Create a TS3 Server Command
                    result = ts_query.command(cmd)  # Execute and Save the Result
                    new_ts = TeamSpeak(ts_server=server.server_id,
                                       ts_admin=session['user_id'],
                                       ts_port=add_teamspeak.teamspeak_port.data,
                                       ts_slot=add_teamspeak.teamspeak_slot.data,
                                       ts_sid=result[0]['sid'],
                                       ts_status=1)
                    db.session.add(new_ts)  # Add TS to Database
                    try:
                        db.session.commit()  # Try to Commit
                        flash("TeamSpeak3 Server Created Successfully! [ Token => {token} ]".format(token=result[0]['token']), "success")
                        if session['is_admin']:  # If User is Admin
                            teamspeaks = db.session.query(TeamSpeak).all()
                        else:  # If User is Reseller
                            teamspeaks = db.session.query(TeamSpeak).filter_by(ts_admin=session['user_id']).all()

                    except IntegrityError as e:  # Except TS Already Exists
                        db.session.rollback()
                        flash(str(e), "danger")
                except (TimeoutError, TS3Error) as e:  # Except (Server Times Out || Login is Wrong)
                    flash(str(e), "danger")
            else:
                flash("Something is wrong with your limits!!", "danger")
        else:  # If user is not admin and he reached his limitatons
            flash("Something went wrong!!", "danger")
    elif request.method == 'POST' and 'search' in request.form:  # If Method is Post & Search Clicked
        if search_form.validate():  # If form is Validated
            if session['is_admin']:  # If User is Admin
                if search_form.search_in.data == 'ts_id':  # If Searching in IDs
                    teamspeaks = db.session.query(TeamSpeak).filter_by(ts_id=search_form.search_query.data).all()
                elif search_form.search_in.data == 'ts_server':  # If Searching in Servers
                    teamspeaks = db.session.query(TeamSpeak).filter_by(ts_server=search_form.search_query.data).all()
                elif search_form.search_in.data == 'ts_port':  # If Searching in Ports
                    teamspeaks = db.session.query(TeamSpeak).filter_by(ts_port=search_form.search_query.data).all()
                elif search_form.search_in.data == 'ts_sid':  # If Searching in SIDs
                    teamspeaks = db.session.query(TeamSpeak).filter_by(ts_sid=search_form.search_query.data).all()
            else:  # If User is Reseller
                if search_form.search_in.data == 'ts_id':  # If Searching in IDs
                    teamspeaks = db.session.query(TeamSpeak).filter_by(ts_id=search_form.search_query.data, ts_admin=session['user_id']).all()
                elif search_form.search_in.data == 'ts_server':  # If Searching in Servers
                    temp = db.session.query(Server.server_id).filter_by(server_ip=search_form.search_query.data).scalar()
                    teamspeaks = db.session.query(TeamSpeak).filter_by(ts_server=temp, ts_admin=session['user_id']).all()
                    del temp
                elif search_form.search_in.data == 'ts_port':  # If Searching in Ports
                    teamspeaks = db.session.query(TeamSpeak).filter_by(ts_port=search_form.search_query.data, ts_admin=session['user_id']).all()
                elif search_form.search_in.data == 'ts_sid':  # If Searching in SIDs
                    teamspeaks = db.session.query(TeamSpeak).filter_by(ts_sid=search_form.search_query.data, ts_admin=session['user_id']).all()
        else:  # If Form is not Validated
            flash("Something went wrong!", "danger")

    return render_template('teamspeaks.html', title="TeamSpeaks", teamspeaks=teamspeaks, add=add_teamspeak, search=search_form)


# Edit a TeamSpeak
@app.route('/teamspeak/edit/<ts_id>', methods=['GET', 'POST'])
@is_logged_in
def edit_teamspeak(ts_id):
    teamspeak = db.session.query(TeamSpeak).filter_by(ts_id=ts_id).first()
    if teamspeak:  # If TeamSpeak Found
        if session['is_admin']:  # If User is Admin
            pass
        elif teamspeak.ts_admin is session['user_id']:  # If User is The Owner of TeamSpeak
            pass
        else:  # If TeamSpeak Not Found
            flash("Access Denied!", "danger")
            return redirect(url_for('teamspeaks'))
    else:  # If TeamSpeak Not Found
        flash("TeamSpeak not Found!", "danger")
        return redirect(url_for("teamspeaks"))

    modify_form = ModifyTeamSpeak(request.form)
    if request.method == 'POST' and 'modify_server' in request.form:  # If Modify Server Clicked.
        if modify_form.validate():  # If Modify Form Validated
            # Check if User is not Admin, User doesn't have slot left, User Slots is not unlimited
            if teamspeak.admin.slot_limit == -1 or teamspeak.admin.is_admin:  # If Owner is Admin or Has an Unlimited Slot Limit
                pass
            elif teamspeak.admin.slot_limit < (teamspeak.admin.used() - int(teamspeak.ts_slot) + int(modify_form.teamspeak_slot.data)):  # Else if TeamSpeak Admin has reached it's slot limit
                flash("You've reached your slot limit! You have {slot_left} left.".format(slot_left=teamspeak.admin.slot_limit-teamspeak.admin.used()), "danger")
                return render_template('editteamspeak.html', title="Edit ", modify=modify_form, teamspeak=teamspeak)

            teamspeak.ts_slot = modify_form.teamspeak_slot.data
            teamspeak.ts_port = modify_form.teamspeak_port.data
            try:  # Try to Connect to TeamSpeak Query and Change the Changes.
                ts_query = TS3Query(teamspeak.server.server_ip, teamspeak.server.server_queryport)
                ts_query.login(teamspeak.server.server_admin, teamspeak.server.server_secure)
                ts_query.use(int(teamspeak.ts_sid))
                cmd = "serveredit virtualserver_port={ts_port} virtualserver_maxclients={ts_slot}".format(ts_port=modify_form.teamspeak_port.data, ts_slot=modify_form.teamspeak_slot.data)
                ts_query.command(cmd)
                try:  # Try to Commit
                    db.session.commit()
                except Exception as e:  # Except any unExcepted Error !!
                    db.session.rollback()
                    flash(str(e), "danger")
                    return redirect(url_for('teamspeaks'))
            except (TS3Error, TimeoutError) as e:  # Except Server Timeout or Wrong Login Info on Query
                flash(str(e), "danger")
                return redirect(url_for('teamspeaks'))

            flash("TeamSpeak Modified Successfully!", "success")
            return redirect(url_for('teamspeaks'))

    elif request.method == 'POST' and 'cancel' in request.form:  # If Cancel Clicked!
        flash("Modify Aborted!", "info")
        return redirect(url_for("teamspeaks"))

    return render_template('editteamspeak.html', title="Edit ", modify=modify_form, teamspeak=teamspeak)


# Delete a TeamSpeak
@app.route('/teamspeak/delete/<ts_id>')
@is_logged_in
def delete_teamspeak(ts_id):
    teamspeak = db.session.query(TeamSpeak).filter_by(ts_id=ts_id).first()
    if teamspeak:  # If TeamSpeak Found
        if session['is_admin']:  # If User is Admin
            pass
        elif teamspeak.ts_admin is session['user_id']:  # If User is Owner of TeamSpeak
            pass
        else:  # If User is not Admin and not Owner of TeamSpeak
            flash("Access Denied!", "danger")
            return redirect(url_for('teamspeaks'))
    else:  # If TeamSpeak Not Found
        flash("TeamSpeak not Found!", "danger")
        return redirect(url_for("teamspeaks"))

    ts_query = TS3Query(teamspeak.server.server_ip, teamspeak.server.server_queryport)
    ts_query.login(teamspeak.server.server_admin, teamspeak.server.server_secure)
    try:  # Try to Stop Server First
        ts_query.command("serverstop sid={ts_sid}".format(ts_sid=teamspeak.ts_sid))
    except TS3Error as e:  # Except ( Server is already offline error )
        if str(e) == "ID 1033 MSG server is not running":  # If Error is what we Excepted, Just pass !
            pass
        else:  # If Error is not what we Excepted, Flash it and Return!
            flash(str(e))
            return redirect(url_for("teamspeaks"))
    ts_query.command("serverdelete sid={ts_sid}".format(ts_sid=teamspeak.ts_sid))
    db.session.query(TeamSpeak).filter_by(ts_id=ts_id).delete()
    db.session.commit()
    flash("Server Deleted Successfully!", "success")
    return redirect(url_for("teamspeaks"))


# Start a TeamSpeak
@app.route('/teamspeak/start/<ts_id>')
@is_logged_in
def start_teamspeak(ts_id):
    teamspeak = db.session.query(TeamSpeak).filter_by(ts_id=ts_id).first()
    if teamspeak:  # If TeamSpeak Found
        if session['is_admin']:  # If User is Admin
            pass
        elif teamspeak.ts_admin is session['user_id']:  # If User is The Owner of TeamSpeak
            pass
        else:  # If User is not Admin and not Owner of TeamSpeak
            flash("Access Denied!", "danger")
            return redirect(url_for('teamspeaks'))
    else:  # If TeamSpeak Not Found
        flash("TeamSpeak not Found!", "danger")
        return redirect(url_for("teamspeaks"))

    if teamspeak.ts_status:  # If TeamSpeak is Online
        flash("Already started!!", "info")
        return redirect(url_for("teamspeaks"))
    else:  # If TeamSpeak is Offline
        ts_query = TS3Query(teamspeak.server.server_ip, teamspeak.server.server_queryport)
        ts_query.login(teamspeak.server.server_admin, teamspeak.server.server_secure)
        cmd = "serverstart sid={ts_sid}".format(ts_sid=teamspeak.ts_sid)
        ts_query.command(cmd)
        ts_query.use(int(teamspeak.ts_sid))
        slots = ts_query.command("serverinfo")[0]['virtualserver_maxclients']
        if teamspeak.admin.slot_limit == -1 or teamspeak.admin.is_admin:  # If Owner is Admin or Owner has an Unlimited Slot Limit
            pass
        elif teamspeak.admin.slot_limit < (
                teamspeak.admin.used() - int(teamspeak.ts_slot) + int(slots)):  # Else If TeamSpeak Owner has Reached it's slot limits!
            flash("You've reached your slot limit! You have {slot_left} left.".format(
                slot_left=teamspeak.admin.slot_limit - teamspeak.admin.used()), "danger")
            cmd = "serverstop sid={ts_sid}".format(ts_sid=teamspeak.ts_sid)
            ts_query.command(cmd)
            ts_query.disconnect()
            return redirect(url_for("teamspeaks"))

        ts_query.disconnect()
        teamspeak.ts_status = 1
        db.session.commit()
        flash("Server Started!", "success")
        return redirect(url_for("teamspeaks"))


# Stop a TeamSpeak
@app.route('/teamspeak/stop/<ts_id>')
@is_logged_in
def stop_teamspeak(ts_id):
    teamspeak = db.session.query(TeamSpeak).filter_by(ts_id=ts_id).first()
    if teamspeak:  # If TeamSpeak Found
        if session['is_admin']:  # If User is Admin
            pass
        elif teamspeak.ts_admin is session['user_id']:  # If User is The Owner of TeamSpeak
            pass
        else:  # If User is not Admin and not Owner of TeamSpeak
            flash("Access Denied!", "danger")
            return redirect(url_for('teamspeaks'))
    else:  # If TeamSpeak Not Found
        flash("TeamSpeak not Found!", "danger")
        return redirect(url_for("teamspeaks"))

    if not teamspeak.ts_status:  # If TeamSpeak is Offline
        flash("Already offline!!", "info")
        return redirect(url_for("teamspeaks"))
    else:  # If TeamSpeak is Online
        ts_query = TS3Query(teamspeak.server.server_ip, teamspeak.server.server_queryport)
        ts_query.login(teamspeak.server.server_admin, teamspeak.server.server_secure)
        cmd = "serverstop sid={ts_sid}".format(ts_sid=teamspeak.ts_sid)
        ts_query.command(cmd)
        ts_query.disconnect()
        teamspeak.ts_status = 0
        teamspeak.ts_slot = 0
        db.session.commit()
        flash("Server Stopped!", "success")
        return redirect(url_for("teamspeaks"))


if __name__ == '__main__':
    app.run()
